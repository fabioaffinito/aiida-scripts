#!/usr/bin/env python

import sys
import argparse
from aiida.plugins import DataFactory
from aiida.tools.dbimporters.plugins.cod import CodDbImporter
from aiida.orm.nodes.data.upf import get_pseudos_from_structure
from aiida.engine import run, submit

importer = CodDbImporter()

def aiida_run(args):

    StructureData = DataFactory('structure')
    ParameterData = DataFactory('dict')
    pseudo_family = 'ZrO2-upf'
    s = load_node(385) 
    parameters = Dict(dict={
              'CONTROL': {
                  'calculation': 'cp',
                  'restart_mode': 'from_scratch',
                  'nstep':10,
                  'iprint':5,
                  'isave':1000000,
                  'dt':1.0,
                  'etot_conv_thr':1.e-8,
                  },
              'SYSTEM': {
                  'ecutrho':200.0,
                  'ecutwfc':30,
                  'ecfixed':25,
                  'qcutz':25,
                  'q2sigma':5,
                  'nr1b':16,
                  'nr2b':16,
                  'nr3b':16,
                  },
              'ELECTRONS': {
                  'emass':800,
                  'emass_cutoff':2.5,
                  'orthogonalization':'ortho',
                  'ortho_eps':2.e-7,
                  'ortho_max':100,
                  'electron_dynamics':'damp',
                  'electron_damping':0.2,
                  'electron_velocities':'zero',
                  },
              'IONS': {
                  'ion_dynamics':'none',
                  'ion_velocities':'zero',
                  'ion_radius(1)':0.8,
                  'ion_radius(2)':0.5,
                  },
                   })
    
    code = load_code(args.codename)

    for nn in args.nodes:
        for nt in args.ntg:
            for nk in args.kpool:
                builder = code.get_builder()
                builder.metadata.label = "ZrO2"
                builder.metadata.description = "QE Benchmark large: CP ZrO2"
                builder.metadata.options.account = "cin_staff"
                builder.metadata.options.resources = {'num_machines': nn,
                                                'num_mpiprocs_per_machine': code.computer.get_default_mpiprocs_per_machine()/args.threads,
                                                'num_cores_per_mpiproc': args.threads}
                builder.metadata.options.custom_scheduler_commands ="export OMP_NUM_THREADS="+str(args.threads)
                builder.metadata.options.queue_name = args.partition
                builder.metadata.options.max_wallclock_seconds = 6 * 60 * 60
                settings_dict = {'cmdline': ['-ntg', str(nt), '-npool', str(nk)]}
                builder.settings = Dict(dict=settings_dict)

                builder.structure = s
                builder.parameters = parameters
                builder.pseudos = get_pseudos_from_structure(s, pseudo_family)
                builder.metadata.store_provenance = True # per fare un run vero sulle macchine
                submit(builder) 

                print(builder)
                print('*******************')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AiiDA job manager')
    parser.add_argument('-c', '--codename', type=str, help='codeATmachine')
    parser.add_argument('-p', '--partition', help='Target partition', choices=['skl_usr_prod', 'skl_dbg_prod'], default='')
    parser.add_argument('-t', '--threads', type=int, help='Number of threads per rank', default=1)
    parser.add_argument('-k', '--kpool', type=int, nargs='+', help='Number of k-point pools', default=[1])
    parser.add_argument('-tg', '--ntg', type=int, nargs='+', help='Number of taskgroups', default=[1])
    parser.add_argument('-m', '--nodes', type=int, nargs='+', help='Number of nodes')
    args = parser.parse_args()
    aiida_run(args)
                                                            
