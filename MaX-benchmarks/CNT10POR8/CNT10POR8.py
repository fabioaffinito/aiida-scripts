#!/usr/bin/env python

#
# Maintainer: Fabio 
#

import sys
import argparse
from aiida.plugins import DataFactory
from aiida.tools.dbimporters.plugins.cod import CodDbImporter
from aiida.orm.nodes.data.upf import get_pseudos_from_structure
from aiida.engine import run, submit

importer = CodDbImporter()

def aiida_run(args):

    StructureData = DataFactory('structure')
    ParameterData = DataFactory('dict')
    KpointsData = DataFactory('array.kpoints')
    pseudo_family = 'CNT10POR8-upf'
    s = load_node(427) 
    parameters = Dict(dict={
              'CONTROL': {
                  'calculation': 'scf',
                  'restart_mode': 'from_scratch',
                  },
              'SYSTEM': {
                  'ecutwfc': 25,
                  'ecutrho':100,
                  'occupations': 'smearing',
                  'smearing':'gaussian', 
                  'degauss':0.02,
                  },
              'ELECTRONS': {
                  'diagonalization':'david',
                  'conv_thr': 1.e-6,
                  'mixing_beta': 0.3,
                  'mixing_mode':'local-TF'
                  }})
    
    kpoints = KpointsData()
    kpoints.set_kpoints_mesh([1,1,1])

    code = load_code(args.codename)
    for nn in args.nodes:
        for nt in args.ntg:
            for nk in args.kpool:
                builder=code.get_builder()
                builder.metadata.label = "CNT10POR8"
                builder.metadata.description = "QE Benchmark CNT10POR8"
                builder.metadata.options.resources = {'num_machines': nn, 
                                                    'num_mpiprocs_per_machine': code.computer.get_default_mpiprocs_per_machine()/args.threads, 
                                                    'num_cores_per_mpiproc': args.threads}
                builder.metadata.options.custom_scheduler_commands ="export OMP_NUM_THREADS="+str(args.threads)
                builder.metadata.options.account = "cin_staff"
                builder.metadata.options.queue_name = args.partition
                builder.metadata.options.max_wallclock_seconds = 6 * 60 * 60
                settings_dict = {'cmdline': ['-ntg', str(nt), '-npool', str(nk)]}
                builder.settings = Dict(dict=settings_dict)

                builder.structure = s
                builder.kpoints = kpoints
                builder.parameters = parameters
                builder.pseudos = get_pseudos_from_structure(s, pseudo_family)
                builder.metadata.store_provenance = True # per fare un run vero sulle macchine
            
                submit(builder) # Da usare solamente assieme a i due comandi di sopra se voglio fare un run vero sulle macchine
            
                print(builder)
                print('*******************')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AiiDA job manager')
    parser.add_argument('-c', '--codename', type=str, help='codeATmachine')
    parser.add_argument('-p', '--partition', help='Target partition', choices=['skl_usr_prod', 'skl_dbg_prod'], default='')
    parser.add_argument('-t', '--threads', type=int, help='Number of threads per rank', default=1)
    parser.add_argument('-k', '--kpool', type=int, nargs='+', help='Number of k-point pools', default=[1])
    parser.add_argument('-tg', '--ntg', type=int, nargs='+', help='Number of taskgroups', default=[1])
    parser.add_argument('-m', '--nodes', type=int, nargs='+', help='Number of nodes')
    args = parser.parse_args()
    aiida_run(args)
