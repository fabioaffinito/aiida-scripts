# coding: utf-8
# maintainer F.Affinito (CINECA)
import sys, os
import tkinter
import matplotlib.pyplot as plt

def get_info(groupname):
    g = Group.get(label=groupname)
    p = {}
    for i,n in enumerate(g.nodes):
      w = n.res.wall_time_seconds-res.init_wall_time_seconds
      cluster_nodes = n.get_option("resources").get("num_machines")
      mpi_per_node = n.get_option("resources").get("num_mpiprocs_per_machine")
      cores_per_mpi = n.get_option("resources").get("num_cores_per_mpiproc")
      total_cores = cluster_nodes*mpi_per_node*cores_per_mpi
      p[total_cores] = w
      print("Nodes, total cores, time",cluster_nodes," , ",total_cores," , ",w)
    plt.style.use('ggplot')
    plt.plot(kind='bar')
    plt.suptitle(n.inputs.code.computer.description +" "+n.inputs.code.description, fontsize=12)
    plt.ylabel('walltime(s)')
    plt.xlabel('number of cores')
    plt.plot(*zip(*sorted(p.items())),'ro')
    plt.show()


if __name__ == '__main__':
    if len(sys.argv) >= 1:
        for name in sys.argv[1:]:
            get_info(name)

    else:
        print("Usage: {} groupname [groupname ...]".format(sys.argv[0]))
