#!/usr/bin/env python

#
# Maintainer: Nicola e Fabio 
#

import sys
from aiida.plugins import DataFactory
from aiida.tools.dbimporters.plugins.cod import CodDbImporter
from aiida.orm.nodes.data.upf import get_pseudos_from_structure
from aiida.engine import run, submit

importer = CodDbImporter()

def aiida_run(codenames):

    StructureData = DataFactory('structure')
    ParameterData = DataFactory('dict') # cambiato parameter in dict
    KpointsData = DataFactory('array.kpoints')
    ###############################
    # This pseudo are from 
    # https://www.physics.rutgers.edu/gbrv/all_pbe_UPF_v1.5.tar.gz
    pseudo_family = 'pbe-paw'
    ###############################

    s = load_node(253) 

    parameters = Dict(dict={
              'CONTROL': {
                  'calculation': 'scf',
                  'restart_mode': 'from_scratch',
                  },
              'SYSTEM': {
                  'ecutwfc': 30,
                  'occupations': 'smearing',
                  'smearing':'mv', 
                  'degauss':0.025,
                  'nspin': 2,
                  'starting_magnetization(1)': +.00,
                  'starting_magnetization(2)':+.00,
                  },
              'ELECTRONS': {
                  'diagonalization':'david',
                  'electron_maxstep':1,
                  'conv_thr': 1.e-5,
                  'mixing_beta': 0.3,
                  'startingwfc':'atomic',
                  'mixing_mode':'local-TF'
                  }})
    
    kpoints = KpointsData()
    kpoints.set_kpoints_mesh([2,2,2])
    
    
    for nn in [4,8,16,64]:

        for codename in codenames:
            code = load_code(codename)
            builder = code.get_builder()
            builder.metadata.label = "GRIR443"
            builder.metadata.description = "QE Benchmark medium: GRIR443"
            builder.metadata.options.resources = {'num_machines': nn, 'num_mpiprocs_per_machine': 8, 'num_cores_per_mpiproc': 4}
            builder.metadata.options.account = "cin_staff"
            builder.metadata.options.queue_name = "gll_usr_prod"
# works with "Galileo" computer and "PWscf/6.4@Galileo"

            builder.metadata.options.max_wallclock_seconds = 60 * 60
            builder.structure = s
            builder.kpoints = kpoints
            builder.parameters = parameters
            builder.pseudos = get_pseudos_from_structure(s, pseudo_family)
        #Le due righe successive dicono che deve essere un run vero. se le imposto al contrario faccio solo un test del codice
        #builder.metadata.dry_run = True # per fare un run vero sulle macchine
            builder.metadata.store_provenance = True # per fare un run vero sulle macchine
            submit(builder) # Da usare solamente assieme a i due comandi di sopra se voglio fare un run vero sulle macchine
        #builder.metadata.dry_run = True # per fare un test locale
        #builder.metadata.store_provenance = False # per fare un test locale        
        #run(builder) # Da usare solamente assieme a i due comandi di sopra se voglio fare un test in locale sul codice.

            print(builder)
            print('*******************')

if __name__ == '__main__':
    if len(sys.argv) >= 1:
        aiida_run(sys.argv[1:])
    else:
        print("Usage: {} codename@machine [codename2@machine2 ...]".format(sys.argv[0]))
