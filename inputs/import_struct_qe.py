from aiida_quantumespresso.tools.pwinputparser import PwInputFile

#input file from which you want to import the structure 
inp = PwInputFile(pwinput='/home/ubuntu/benchmarks-master/Quantum_Espresso/PW/Small/AUSURF112/ausurf.in')

StructureData = inp.get_structuredata()

StructureData.store()
