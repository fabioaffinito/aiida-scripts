#!/usr/bin/env python

#
# Maintainer: Pietro Bonfa'
#

import sys
from aiida.orm import Group, Code, DataFactory, load_node, JobCalculation
from aiida.orm.querybuilder import QueryBuilder
from aiida.tools.dbimporters.plugins.cod import CodDbImporter
importer = CodDbImporter()

def run(codenames):
    StructureData = DataFactory('structure')
    ParameterData = DataFactory('parameter')
    KpointsData = DataFactory('array.kpoints')
    ###############################
    # This pseudo are from 
    # http://www.quantum-simulation.org/potentials/sg15_oncv/sg15_oncv_upf_2015-10-07.tar.gz
    pseudo_family = 'sg15_oncv'
    ###############################

    results = importer.query(id=1010421)
    s = results[0].get_aiida_structure()
    parameters = ParameterData(dict={
              'CONTROL': {
                  'calculation': 'vc-relax',
                  'restart_mode': 'from_scratch',
                  },
              'SYSTEM': {
                  'ecutwfc': 100.,
                  'smearing': 'mp',
                  'occupations': 'smearing',
                  'degauss': 0.001,
                  'vdw_corr': 'dft-d3',
                  },
              'ELECTRONS': {
                  'conv_thr': 1.e-9,
                  'mixing_beta': 0.5,
                  }})
    
    kpoints = KpointsData()
    kpoints.set_kpoints_mesh([8,8,4])
    
    for codename in codenames:
        code = Code.get_from_string(codename)
        calc = code.new_calc(max_wallclock_seconds=7200,
            resources={"num_machines": 1})
        calc.label = "BiBrO"
        calc.description = "Testing pw.x with BiBrO: NC pseudo, kpoint, vdw_corr='dft-d3', relaxation"

        calc.use_structure(s)
        calc.use_code(code)
        calc.use_parameters(parameters)
        calc.use_kpoints(kpoints)
        calc.use_pseudos_from_family(pseudo_family)

        calc.store_all()
        print ("created calculation with PK={}".format(calc.pk))
        group, created = Group.get_or_create(name="BiBrO")
        group.add_nodes(calc)
        calc.submit()



if __name__ == '__main__':
    if len(sys.argv) > 1:
        run(sys.argv[1:])
    else:
        print("Usage: {} codename@machine [codename2@machine2 ...]".format(sys.argv[0]))

