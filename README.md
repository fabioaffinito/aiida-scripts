This repository contains some scripts to be used inside AiiDA to dump data from simulations for validation and benchmarking purposes within the MaX project.

/inputs/ contains scripts to submit QuantumESPRESSO calculations inside AiiDA
/deploy/ contains scripts to manipulate and display results of simulations stored in AiiDA

benchmarks.py plots the walltime vs number of cores for all the QuantumESPRESSO simulations belonging to the same AiiDA group

*How to use*

At present there are a few test cases, whose structures and related pseudopotentials are already imported in AiiDA (note 
that references are made inside the input scripts).
The scripts for inputs should be modified when working on different AiiDA instances.
In order to import a structure into AiiDA starting from a Quantum ESPRESSO input the following commands can be executed 
from the verdi shell:
from aiida_quantumespresso.tools.pwinputparser import PwInputFile

#input file from which you want to import the structure

inp = PwInputFile(pwinput='/home/ubuntu/benchmarks-master/Quantum_Espresso/PW/Small/AUSURF112/ausurf.in')

StructureData = inp.get_structuredata()

StructureData.store()

Once the input file is adapted, the calculation can be submitted using:

verdi run <input-file> <machine-file>

Note that the input scripts are prepared in order to submit multiple calculations changing the number of nodes using 
this outer loop:

for nn in [4,8,12,16,32]: <- change this to change the number of nodes

When the execution is complete, you can add to a group all the calculation that you want to plot together (for example, 
the same calculation running on different number of nodes/cores):

verdi group add-nodes 265 273 274 275 276 -G 28

Afterwards, you can use the script benchmarks.py to run a plot of the results of the simulations belonging to 
the group:

verdi run benchmarks.py <name-of-the-group> 
